#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

include /usr/share/dpkg/default.mk

OVERRIDE_CFLAGS = $(CFLAGS)
OVERRIDE_CPPFLAGS = $(CPPFLAGS)
OVERRIDE_LDFLAGS = $(LDFLAGS)
export OVERRIDE_CFLAGS OVERRIDE_CPPFLAGS OVERRIDE_LDFLAGS

V=
ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
	V=--verbose
endif

HOME = $(CURDIR)/debian/fakehome
KITTY_RUNTIME_DIRECTORY = $(CURDIR)/debian/fakeruntime

%:
	dh $@ --with python3 --with sphinxdoc

override_dh_auto_clean:
	python3 setup.py $(V) clean

override_dh_auto_build:
	env HOME="$(HOME)" PATH=$$PATH:$(CURDIR)/debian/bin python3 setup.py $(V) linux-package

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	mkdir -p "$(HOME)"
	mkdir -p "$(KITTY_RUNTIME_DIRECTORY)"
	env HOME="$(HOME)" KITTY_RUNTIME_DIRECTORY="$(KITTY_RUNTIME_DIRECTORY)" python3 setup.py $(V) build-launcher
	env HOME="$(HOME)" KITTY_RUNTIME_DIRECTORY="$(KITTY_RUNTIME_DIRECTORY)" LC_ALL=C.UTF-8 python3 setup.py $(V) test
endif

override_dh_auto_install:
	mkdir -p $(CURDIR)/debian/tmp/usr
	cp -a linux-package/* $(CURDIR)/debian/tmp/usr/

override_dh_install:
	dh_install -Nkitty-shell-integration -Xlib/kitty/shell-integration
	dh_install -pkitty-shell-integration

override_dh_installdocs:
	dh_installdocs
	# Remove PayPal's tracking pixel
	[ ! -d debian/kitty-doc ] || sed -i '/pixel\.gif/d' debian/kitty-doc/usr/share/doc/kitty/html/support.html

override_dh_python3:
	dh_python3 --no-ext-rename

override_dh_installchangelogs:
	dh_installchangelogs docs/changelog.rst

override_dh_compress:
	dh_compress -X/html/
